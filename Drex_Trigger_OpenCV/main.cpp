#include "opencv2/opencv.hpp"

using namespace cv;

int main(int, char**)
{
    VideoCapture cap(1); // open the default camera
    if(!cap.isOpened())  // check if we succeeded
        return -1;

    //Mat edges;
    namedWindow("edges",1);
    for(;;)
    {
        Mat frame;
        cap >> frame; // get a new frame from camera
        //cvtColor(frame, edges, COLOR_BGR2GRAY);
        //GaussianBlur(edges, edges, Size(7,7), 1.5, 1.5);
        //Canny(edges, edges, 0, 30, 3);
        //imshow("edges", edges);
		imshow("edges", frame);
        if(waitKey(30) >= 0) break;
    }
    // the camera will be deinitialized automatically in VideoCapture destructor
    return 0;
}



//#include <iostream>
//#include <opencv2/opencv.hpp>
////#include <tcambin.h>
////#include <vector>
////#include <gst/gst.h>

//using namespace std;
//using namespace cv;

//int main(){

//  Mat image_1; // = Mat::ones(3856, 2764, CV_8UC1);
//  //Mat image_2; // = Mat::ones(3856, 2764, CV_8UC1);

//  VideoCapture stream_1;//, //stream_2;

//  while(true){
	
//	//namedWindow("image 1", WINDOW_AUTOSIZE);
//	//namedWindow("image 2", WINDOW_AUTOSIZE);
//	//waitKey();
	
//	stream_1.open(1);
////	stream_1.set(CV_CAP_PROP_FRAME_WIDTH, 3856);
////	stream_1.set(CV_CAP_PROP_FRAME_HEIGHT, 2764);
//	stream_1 >> image_1;
	
//	//stream_2.open(2);
////	stream_2.set(CV_CAP_PROP_FRAME_WIDTH, 3856);
////	stream_2.set(CV_CAP_PROP_FRAME_HEIGHT, 2764);
//	//stream_2 >> image_2;
	
//	imshow("image 1", image_1);
//	//imwrite("image_1.png", image_1);
	
//	//imshow("image 2", image_2);
//	//imwrite("image_2.png", image_2);
//	waitKey();
//	return 0;

//    }

//}

