TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp

INCLUDEPATH += /usr/include/gstreamer-1.0
INCLUDEPATH += /usr/include/glib-2.0
INCLUDEPATH += /usr/lib/x86_64-linux-gnu/glib-2.0/include
INCLUDEPATH += /usr/lib/x86_64-linux-gnu/gstreamer-1.0/include

DEPENDPATH += /usr/include/gstreamer-1.0
DEPENDPATH += /usr/include/glib-2.0
DEPENDPATH += /usr/lib/x86_64-linux-gnu/glib-2.0/include
DEPENDPATH += /usr/lib/x86_64-linux-gnu/gstreamer-1.0/include

LIBS += -lopencv_core -lopencv_highgui -lopencv_imgcodecs -lopencv_videoio

HEADERS += \
    tcambin.h
