#ifndef _GIGECAMERA_H
#define _GIGECAMERA_H
#include <string>
#include <gst/gst.h>
#include <glib.h>

// Definition for the callback function
typedef gboolean (*BUFFER_CALLBACK_T) (GstElement *image_sink, GstBuffer *buffer, GstPad *pad, void *appdata);


bool OpenCamera(const char* SerialNumber);
void CloseCamera();


/*
 * Open and start the camera given by name. Set the video format and resolution.
 * ToDo: Function for enumeting video formats and frame rates
 */
int start_camera( int width, int height, int fpsnominator, int fpsdenomitor,int showvideo, int argc, char** argv );

/*
 * Stop the camera
 */
void stop_camera();

/*
 * Start the autoc focus one push
 */ 
void trigger_autofocus_camera();


/*
 * Set a camera property, e.g. "Zoom", "Focus".
 */ 
void set_property_camera(const char* Property, const int Value );

void set_property_camera_enum(const char *Property, const int Value );

/*
 * Save an image.
 */
int  save_image(const char* Filename);


void initLibrary( int argc, char** argv );

std::string get_camera_serial( int Index );
std::string get_camera_name( int Index );
std::string get_VideoFormat( int Index );
std::string get_FrameRate(int VideoFormatIndex, int FrameRateIndex );

int get_VideoFormatResolution_FramRate( int VideoFormatIndex, int FrameRateIndex , int* width, int* height, int *numerator, int *denominator);

void list_Properties();


// Set a callback function, that is called for each incoming frame
void SetCallback( BUFFER_CALLBACK_T cbfunc, void* Data); 


#endif