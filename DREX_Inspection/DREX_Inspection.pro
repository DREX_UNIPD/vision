TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp

LIBS += -lopencv_core
LIBS += -lopencv_highgui
LIBS += -lopencv_imgproc
LIBS += -lopencv_videoio

INCLUDEPATH += $$PWD/../../../../../opt/imperx/bobcat_gev/include
DEPENDPATH += $$PWD/../../../../../opt/imperx/bobcat_gev/include

unix:!macx: LIBS += -L$$PWD/../../../../../opt/imperx/bobcat_gev/lib/ -lEbTransportLayerLib
unix:!macx: LIBS += -L$$PWD/../../../../../opt/imperx/bobcat_gev/lib/ -lEbUtilsLib
unix:!macx: LIBS += -L$$PWD/../../../../../opt/imperx/bobcat_gev/lib/ -lipxtruesense
unix:!macx: LIBS += -L$$PWD/../../../../../opt/imperx/bobcat_gev/lib/ -lPtConvertersLib
unix:!macx: LIBS += -L$$PWD/../../../../../opt/imperx/bobcat_gev/lib/ -lPtUtilsLib
unix:!macx: LIBS += -L$$PWD/../../../../../opt/imperx/bobcat_gev/lib/ -lPvAppUtils
unix:!macx: LIBS += -L$$PWD/../../../../../opt/imperx/bobcat_gev/lib/ -lPvBase
unix:!macx: LIBS += -L$$PWD/../../../../../opt/imperx/bobcat_gev/lib/ -lPvBuffer
unix:!macx: LIBS += -L$$PWD/../../../../../opt/imperx/bobcat_gev/lib/ -lPvDevice
unix:!macx: LIBS += -L$$PWD/../../../../../opt/imperx/bobcat_gev/lib/ -lPvGenICam
unix:!macx: LIBS += -L$$PWD/../../../../../opt/imperx/bobcat_gev/lib/ -lPvGUI
unix:!macx: LIBS += -L$$PWD/../../../../../opt/imperx/bobcat_gev/lib/ -lPvPersistence
unix:!macx: LIBS += -L$$PWD/../../../../../opt/imperx/bobcat_gev/lib/ -lPvSerial
unix:!macx: LIBS += -L$$PWD/../../../../../opt/imperx/bobcat_gev/lib/ -lPvStream
unix:!macx: LIBS += -L$$PWD/../../../../../opt/imperx/bobcat_gev/lib/ -lPvTransmitter
unix:!macx: LIBS += -L$$PWD/../../../../../opt/imperx/bobcat_gev/lib/ -lPtUtilsLib
unix:!macx: LIBS += -L$$PWD/../../../../../opt/imperx/bobcat_gev/lib/ -lPvVirtualDevice
unix:!macx: LIBS += -L$$PWD/../../../../../opt/imperx/bobcat_gev/lib/ -lSimpleImagingLib

