TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp

INCLUDEPATH += \
	/usr/include/gstreamer-1.0 \
	/usr/include/glib-2.0 \
	/usr/lib/aarch64-linux-gnu/glib-2.0/include \
	/usr/lib/aarch64-linux-gnu/gstreamer-1.0/include

LIBS += \
	/usr/lib/aarch64-linux-gnu/libgstreamer-1.0.so \
	/usr/lib/aarch64-linux-gnu/libgobject-2.0.so \
	/usr/lib/aarch64-linux-gnu/libglib-2.0.so \
	/usr/lib/libtcam.so \
	/usr/lib/libtcamprop.so \
