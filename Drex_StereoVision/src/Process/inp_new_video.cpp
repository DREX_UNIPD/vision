//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		16/09/17
//	Descrizione:		Notifica salvataggio di un nuovo video
//****************************************************************************************************//

// Header del processo
#include <Process/inp.h>

using namespace std;
using namespace drex::processes;

// Notifica
void INProcess::newVideo(QString path) {
	_logger->info(string("New video saved in: \"") + path.toStdString() + string("\""));
	
	// Segnalo la presenza della nuova immagine
	Message msg;
	msg += "NEW VIDEO";
	msg += (string("FILEPATH = ") + path.toStdString()).c_str();
	_signaling_socket.socket->sendMessage(msg);
}

