//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		09/08/2017
//	Descrizione:		Inizializzazione delle camere
//****************************************************************************************************//

// Header del processo
#include <Process/inp.h>

using namespace std;
using namespace drex::processes;

bool INProcess::initCameras() {
	// Verifico se almeno una camera è configurata e abilitata
	const YAML::Node &config_cameras = _configuration["Cameras"];
	
	_cameras_enabled = false;
	
	for (size_t i = 0; i < config_cameras.size(); i++) {
		const YAML::Node &current_camera = config_cameras[i];
		if (current_camera["Enabled"].as<bool>()) {
			_cameras_enabled = true;
		}
	}
	
	return true;
}
