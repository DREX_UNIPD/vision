//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		06/08/2017
//	Descrizione:		Stato Run: il processo acquisisce le immagini continuativamente e le salva
//						in formato video con durata predeterminata.
//****************************************************************************************************//

// Header del processo
#include <Process/inp.h>

#define _UNIX_

// Header Pleora
#include <PvSampleUtils.h>
#include <PvDevice.h>
#include <PvDeviceGEV.h>
#include <PvDeviceU3V.h>
#include <PvStream.h>
#include <PvStreamGEV.h>
#include <PvStreamU3V.h>
#include <PvBuffer.h>
#include <PvSystem.h>
#include <PvBufferLib.h>
#include <PvBufferWriter.h>

// Header DREX
#include <Camera/inp_camera_worker.h>

using namespace std;
using namespace drex::camera;
using namespace drex::processes;

// Inizializzazione dello stato
void INProcess::stateRunInit() {
	_logger->info("RUN: Init");
	
	// Pulisco la lista delle camere
//	for (int i = 0; i < _cameras.size(); i++) {
//		Camera *c;
//		c = _cameras[i];
		
//		QObject::disconnect(c, &Camera::gotNewImage, this, &INProcess::newImage);
//		QObject::disconnect(this, &INProcess::stopAcquire, c, &Camera::stopAcquisition);
//	}
	
	// Verifico se almeno una camera è abilitata
	if (!_cameras_enabled) {
		_logger->warn("No camera configured");
		return;
	}
	
	// Individuazione delle camere e inizializzazione della classe corrispondente
	PvSystem lSystem;
	vector<const PvDeviceInfo *> lDIVector;
	
	// Trovo tutte le interfacce di sistema
	lSystem.Find();
	
	// Trovo i dispositivi nelle interfacce disponibili
	for (uint32_t i = 0; i < lSystem.GetInterfaceCount(); i++) {
		const PvInterface *lInterface = dynamic_cast<const PvInterface *>(lSystem.GetInterface(i));
		if (lInterface != NULL) {
			_logger->info(string("Interface detected: ") + string(lInterface->GetDisplayID().GetAscii()));
			for (uint32_t j = 0; j < lInterface->GetDeviceCount(); j++) {
				const PvDeviceInfo *lDI = dynamic_cast<const PvDeviceInfo *>(lInterface->GetDeviceInfo(j));
				if (lDI != NULL) {
					lDIVector.push_back(lDI);
					_logger->info(string("Device detected: ") + string(lDI->GetDisplayID().GetAscii()));
				}
			}
		}
	}
	
	// Verifico di aver trovato almeno un dispositivo
	if (lDIVector.size() == 0) {
		_logger->warn("No device found!");
		return;
	}
	
	// Configurazione delle camere
	const YAML::Node &config_cameras = _configuration["Cameras"];
	
	// Dispositivo corrente
	const PvDeviceInfoGEV *lDeviceGEV;
	
	// Associo i dispositivi trovati alle camere
	bool done = false;
	for (size_t i = 0; i < lDIVector.size(); i++) {
		lDeviceGEV = dynamic_cast<const PvDeviceInfoGEV *>(lDIVector[i]);
		if (lDeviceGEV == NULL) continue;
		
		// Ritrovo l'ip del dispositivo corrente
		string ip;
		ip = lDeviceGEV->GetIPAddress().GetAscii();
		
		for (size_t j = 0; j < config_cameras.size(); j++) {
			const YAML::Node &current_camera = config_cameras[j];
			if (current_camera["Enabled"].as<bool>() && current_camera["IP"].as<string>() == ip) {
				QString name;
				QString storage_path;
				bool capture_video;
				
				name = QString::fromStdString(current_camera["Name"].as<string>());
				storage_path = _storage_video_dir.absoluteFilePath(name);
				capture_video = current_camera["Video"].as<bool>();
				
				_logger->info(string("Creating camera: IP = ") + ip + string(", Storage Path = \"") + storage_path.toStdString() + string("\", ConnectionID = ") + string(lDeviceGEV->GetConnectionID().GetAscii()));
				
				QThread *thread = new QThread;
				CameraWorker *worker;
				worker = new CameraWorker(
					name,
					capture_video,
					lDeviceGEV->GetConnectionID(),
					_storage_video_dir,
					_storage_image_dir,
					_storage_ram_dir,
					_max_video_time,
					_max_video_size
				);
				worker->moveToThread(thread);
				
				QObject::connect(thread, &QThread::started, worker, &CameraWorker::startAcquire, Qt::QueuedConnection);
				QObject::connect(worker, &CameraWorker::finished, thread, &QThread::quit, Qt::QueuedConnection);
				QObject::connect(worker, &CameraWorker::finished, worker, &CameraWorker::deleteLater, Qt::QueuedConnection);
				QObject::connect(thread, &QThread::finished, thread, &QThread::deleteLater, Qt::QueuedConnection);
				
				QObject::connect(this, &INProcess::stopAcquire, worker, &CameraWorker::stopAcquire, Qt::QueuedConnection);
				QObject::connect(this, &INProcess::acquireNext, worker, &CameraWorker::acquireNext, Qt::QueuedConnection);
				QObject::connect(worker, &CameraWorker::newImage, this, &INProcess::newImage, Qt::QueuedConnection);
				QObject::connect(worker, &CameraWorker::newVideo, this, &INProcess::newVideo, Qt::QueuedConnection);
				
				thread->start();
				
				done = true;
				break;
			}
		}
		
		if (done) break;
	}
	
	
//	for (int i = 0; i < _cameras.size(); i++) {
//		Camera *&c = _cameras[i];
//		c->startAcquisition();
//	}
}

// Pulizia dello stato
void INProcess::stateRunCleanup() {
	_logger->info("RUN: Cleanup");
	
	// Fermo l'acquisizione
	emit stopAcquire();
}
