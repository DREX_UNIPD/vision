//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		13/09/17
//	Descrizione:		Classe per la gestione di una camera in un thread separato
//****************************************************************************************************//

#ifndef INP_CAMERA_WORKER_H
#define INP_CAMERA_WORKER_H

// Header Qt
#include <QtCore>

// Header Pleora
#include <PvStream.h>
#include <PvDeviceInfoGEV.h>
#include <PvBufferWriter.h>

// Header per logging
#include <spdlog/spdlog.h>

// Header OpenCV
#include <opencv2/videoio.hpp>

namespace drex {
namespace camera {

class CameraWorker : public QObject {
	Q_OBJECT
	
private:
	using Logger = spdlog::logger;
	using BufferList = std::list<PvBuffer *>;
	
	// Variabili private
	
	// Logger
	std::shared_ptr<Logger> _logger;
	
	// Camera
	PvDevice *_device;
	PvStream *_stream;
	PvString _connection_id;
	BufferList *_buffer_list;
	
	PvGenCommand *_cmd_start;
	PvGenCommand *_cmd_stop;
	
	PvGenFloat *_parameter_frame_rate;
	PvGenFloat *_parameter_bandwidth;
	
	PvBufferWriter *_buffer_writer;
	
	cv::VideoWriter _video_writer;
	
	// Worker
	QString _camera_name;
	bool _capture_video;
	
	bool _acquiring;
	bool _acquire_next;
	QString _acquire_destination;
	QDir _video_dir;
	QString _video_file;
	QDir _image_dir;
	QDir _temp_dir;
	QString _temp_image_file;
	
	uint64_t _max_video_size;
	uint64_t _max_video_time;
	
	uint32_t _frame_counter;
	
	// Metodi
	bool connectToDevice(const PvString &connection_id);
	bool openStream(const PvString &connection_id);
	void configureStream(PvDevice *device, PvStream *stream);
	void createStreamBuffers(PvDevice *device, PvStream *stream, BufferList *buffer_list);
	void freeStreamBuffers(BufferList *buffer_list);
	
	// Costanti
	static constexpr uint32_t BUFFER_COUNT = 2;
	static constexpr uint32_t FRAME_LIMIT = 60 * 24; // 24 fps
	
	// Metodi ausiliari
	
	// Calcolo prossimo nome valido
	QString _getName(QDir directory, QString before, QDateTime time, QString after);
	QString _saveVideo();
	void _startVideo();
	
public:
	// Costruttore
	CameraWorker(
		QString camera_name,
		bool capture_video,
		PvString connection_id,
		QDir video_dir,
		QDir image_dir,
		QDir temp_dir,
		uint64_t max_video_time,
		uint64_t max_video_size
	);
	
	// Inizio acquisizione delle immagini
	void startAcquire();
	
	// Distruttore
	~CameraWorker();
	
signals:
	// Nuova immagine disponibile
	void newImage(QString path, double bandwidth, double framerate);
	
	// Nuovo video disponibile
	void newVideo(QString path);
	
	// Fine acquisizione
	void finished();
	
private slots:
	// Singola acquisizione
	void acquire();
	
public slots:
	// Stop acquisizione
	void stopAcquire();
	
	// Acquisisci la prossima immagine
	void acquireNext(QString name);
};

}
}

#endif // INP_CAMERA_WORKER_H

