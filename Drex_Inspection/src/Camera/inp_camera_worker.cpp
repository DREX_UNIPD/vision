//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		13/09/17
//	Descrizione:		Classe per la gestione di una camera in un thread separato
//****************************************************************************************************//

// Header della classe
#include <Camera/inp_camera_worker.h>

#define _UNIX_

// Header Pleora
#include <PvSampleUtils.h>
#include <PvDevice.h>
#include <PvDeviceGEV.h>
#include <PvDeviceU3V.h>
#include <PvStream.h>
#include <PvStreamGEV.h>
#include <PvStreamU3V.h>
#include <PvBuffer.h>
#include <PvSystem.h>
#include <PvBufferLib.h>

// DREX
#include <PvBufferWriter.h>

// Header OpenCV
#include <opencv2/highgui.hpp>
#include <opencv2/imgcodecs.hpp>

using namespace cv;
using namespace std;
using namespace drex::camera;

typedef std::list<PvBuffer*> BufferList;

// Costruttore
CameraWorker::CameraWorker(QString camera_name, bool capture_video, PvString connection_id, QDir video_dir, QDir image_dir, QDir temp_dir, uint64_t max_video_time, uint64_t max_video_size) :
	_connection_id(connection_id),
	_camera_name(camera_name),
	_capture_video(capture_video),
	_acquiring(false),
	_acquire_next(false),
	_video_dir(video_dir),
	_image_dir(image_dir),
	_temp_dir(temp_dir),
	_max_video_size(max_video_size),
	_max_video_time(max_video_time) {
	// Inizializzazione della camera
	_logger = spdlog::get("logger");
	_buffer_writer = new PvBufferWriter();
	_buffer_list = new BufferList();
	_cmd_start = nullptr;
	_cmd_stop = nullptr;
	_parameter_frame_rate = nullptr;
	_parameter_bandwidth = nullptr;
	_frame_counter = 0;
}

// Calcolo prossimo nome valido
QString CameraWorker::_getName(QDir directory, QString before, QDateTime time, QString after) {
	QString time_str;
	QString file_name;
	QFileInfo dest_file;
	
	// Costruisco il nome del file
	time_str = time.toString("yyyy_MM_dd__HH_mm_ss");
	file_name = before + time_str + after;
	dest_file.setFile(directory.absoluteFilePath(file_name));
	
	// Trovo il primo nome disponibile
	int i = 0;
	while (dest_file.exists()) {
		file_name = before + time_str + QString("__") + QString::number(i) + after;
		dest_file.setFile(directory.absoluteFilePath(file_name));
		i++;
	}
	
	return directory.absoluteFilePath(file_name);
}

void CameraWorker::_startVideo() {
	_video_writer.open(_video_file.toStdString(), VideoWriter::fourcc('M', 'J', 'P', 'G'), 24, Size(1392, 1040));
}

QString CameraWorker::_saveVideo() {
	_logger->info("Saving video");
	QString file_name;
	_video_writer.release();
	
	file_name = _getName(_video_dir, "video_", QDateTime::currentDateTime(), ".avi");
	
	if (!QFile::rename(_video_file, file_name)) {
		_logger->info("Failed to save video");
	}
	
	return file_name;
}

// Inizio acquisizione delle immagini
void CameraWorker::startAcquire() {
	// Mi connetto al dispositivo
	if (!connectToDevice(_connection_id)) return;
	
	// Creo lo stream
	if (!openStream(_connection_id)) return;
	
	// Configuro lo stream
	configureStream(_device, _stream);
	
	// Creo i buffer
	createStreamBuffers(_device, _stream, _buffer_list);
	
	// Get device parameters need to control streaming
	PvGenParameterArray *lDeviceParams = _device->GetParameters();
	
	// Map the GenICam AcquisitionStart and AcquisitionStop commands
	_cmd_start = dynamic_cast<PvGenCommand *>(lDeviceParams->Get("AcquisitionStart"));
	_cmd_stop = dynamic_cast<PvGenCommand *>(lDeviceParams->Get("AcquisitionStop"));
	
	// Get stream parameters
	PvGenParameterArray *lStreamParams = _stream->GetParameters();
	
	// Map a few GenICam stream stats counters
	_parameter_frame_rate = dynamic_cast<PvGenFloat *>(lStreamParams->Get("AcquisitionRate"));
	_parameter_bandwidth = dynamic_cast<PvGenFloat *>(lStreamParams->Get("Bandwidth"));
	
	// Enable streaming and send the AcquisitionStart command
	
	_device->StreamEnable();
	_cmd_start->Execute();
	
	_logger->info("Acquisition start ended");
	
	_video_file = _video_dir.absoluteFilePath("video.avi");
	
	if (_capture_video) {
		_startVideo();
	}
	
	_temp_image_file = _temp_dir.absoluteFilePath("image.bmp");
	
	QTimer::singleShot(0, this, &CameraWorker::acquire);
	_acquiring = true;
}

// Singola acquisizione
void CameraWorker::acquire() {
	// Verifico se sto ancora acquisendo
	if (!_acquiring) {
		_logger->info("Not acquiring, stopping timer");
		emit finished();
		return;
	}
	
	double lFrameRateVal = 0.0;
	double lBandwidthVal = 0.0;
	
	PvBuffer *lBuffer = NULL;
	PvResult lOperationResult;

	// Retrieve next buffer
	PvResult lResult = _stream->RetrieveBuffer(&lBuffer, &lOperationResult, 1000);
	if (lResult.IsOK()) {
		if (lOperationResult.IsOK()) {
			if (lBuffer->GetPayloadType() == PvPayloadTypeImage) {
				_logger->info(string("Frame captured (") + to_string(_frame_counter) + ")");
				
				_parameter_frame_rate->GetValue(lFrameRateVal);
				_parameter_bandwidth->GetValue(lBandwidthVal);
				
				// Conversione tra Pleora e OpenCV
				Mat curimg;
				if (_capture_video || _acquire_next) {
					_buffer_writer->Store(lBuffer, _temp_image_file.toStdString().c_str());
					curimg = imread(_temp_image_file.toStdString());
				}
				
				// Se sto catturando il video
				if (_capture_video) {
					_video_writer.write(curimg);
					
					// Se ho superato il limite di dimensione salvo il file e ne apro un altro
					_frame_counter++;
					if (_frame_counter >= FRAME_LIMIT) {
						_frame_counter = 0;
						emit newVideo(_saveVideo());
						_startVideo();
					}
				}
				
				if (_acquire_next) {
					_acquire_next = false;
					
					// Salvo l'immagine in JPG
					QString file_name;
					file_name = _getName(_image_dir, "image_", QDateTime::currentDateTime(), ".jpg");
					if (!imwrite(file_name.toStdString(), curimg)) {
						_logger->info("Failed to save image");
					}
					
					// Notifico l'avvenuto salvataggio
					emit newImage(file_name, lBandwidthVal, lFrameRateVal);
				}
			}
		}
		else {
			string error;
			error += "Operational result not OK: ";
			error += lOperationResult.GetCodeString().GetAscii();
			_logger->info(error);
		}
	}
	else {
		string error;
		error += "Result not OK: ";
		error += lResult.GetCodeString().GetAscii();
		_logger->info(error);
	}
	
	if (!_acquiring) {
		_logger->info("Not acquiring, stopping timer.");
		emit finished();
		return;
	}
	
	// Re-queue the buffer in the stream object
	_stream->QueueBuffer(lBuffer);
	
	// Faccio ripartire l'acquisizione
	QTimer::singleShot(0, this, &CameraWorker::acquire);
}

// Acquisisci la prossima immagine
void CameraWorker::acquireNext(QString name) {
	if (name.toUpper() != "ALL" && name.toUpper() != _camera_name.toUpper()) return;
	_acquire_next = true;
}

// Stop acquisizione
void CameraWorker::stopAcquire() {
	if (!_acquiring) {
		return;
	}
	
	_logger->info("Acquisition stopped");
	
	// Salvo il video in corso
	if (_capture_video) {
		emit newVideo(_saveVideo());
	}
	
	// Tell the device to stop sending images.
	_cmd_stop->Execute();
	
	// Disable streaming on the device
	_device->StreamDisable();
	
	// Abort all buffers from the stream and dequeue
	_stream->AbortQueuedBuffers();
	while (_stream->GetQueuedBufferCount() > 0) {
		PvBuffer *lBuffer = NULL;
		PvResult lOperationResult;
		
		_stream->RetrieveBuffer(&lBuffer, &lOperationResult);
	}
	
	// Rilascio i buffer allocati
	freeStreamBuffers(_buffer_list);
	
	// Close the stream
	_stream->Close();
	PvStream::Free(_stream);
	
	// Disconnect the device
	_device->Disconnect();
	PvDevice::Free(_device);
	
	_cmd_start = nullptr;
	_cmd_stop = nullptr;
	_parameter_frame_rate = nullptr;
	_parameter_bandwidth = nullptr;
	
	_acquiring = false;
}

bool CameraWorker::connectToDevice(const PvString &connection_id) {
	PvResult lResult;
	_device = NULL;
	_device = PvDevice::CreateAndConnect(connection_id, &lResult);
	if (_device == NULL){
		_logger->error("Unable to connect to device");
		return false;
	}
	return true;
}
bool CameraWorker::openStream(const PvString &connection_id) {
	PvResult lResult;
	_stream = NULL;
	_stream = PvStream::CreateAndOpen(connection_id, &lResult);
	if (_stream == NULL) {
		_logger->error("Unable to stream from device");
		return false;
	}
	return true;
}
void CameraWorker::configureStream(PvDevice *device, PvStream *stream) {
	PvDeviceGEV* lDeviceGEV = dynamic_cast<PvDeviceGEV *>(device);
	if (lDeviceGEV != NULL) {
		PvStreamGEV *lStreamGEV = static_cast<PvStreamGEV *>(stream);
		lDeviceGEV->NegotiatePacketSize();
		lDeviceGEV->SetStreamDestination(lStreamGEV->GetLocalIPAddress(), lStreamGEV->GetLocalPort());
	}
}
void CameraWorker::createStreamBuffers(PvDevice *device, PvStream *stream, BufferList *buffer_list) {
	// Reading payload size from device
	uint32_t lSize = device->GetPayloadSize();
	
	// Use BUFFER_COUNT or the maximum number of buffers, whichever is smaller
	uint32_t lBufferCount = (stream->GetQueuedBufferMaximum() < BUFFER_COUNT) ? stream->GetQueuedBufferMaximum() : BUFFER_COUNT;
	
	// Allocate buffers
	for (uint32_t i = 0; i < lBufferCount; i++) {
		// Create new buffer object
		PvBuffer *lBuffer = new PvBuffer;
	
		// Have the new buffer object allocate payload memory
		lBuffer->Alloc(static_cast<uint32_t>(lSize));
		
		// Add to external list - used to eventually release the buffers
		buffer_list->push_back(lBuffer);
	}
	
	// Queue all buffers in the stream
	BufferList::iterator lIt = buffer_list->begin();
	while (lIt != buffer_list->end()) {
		stream->QueueBuffer(*lIt);
		lIt++;
	}
}
void CameraWorker::freeStreamBuffers(BufferList *buffer_list) {
	// Go through the buffer list
	BufferList::iterator lIt = buffer_list->begin();
	while (lIt != buffer_list->end()) {
		delete *lIt;
		lIt++;
	}
	
	// Clear the buffer list 
	buffer_list->clear();
}

// Distruttore
CameraWorker::~CameraWorker() {
	// Chiusura della connessione con la camera
	delete _buffer_writer;
	delete _buffer_list;
}
