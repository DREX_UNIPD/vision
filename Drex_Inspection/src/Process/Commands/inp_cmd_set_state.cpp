//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		06/08/2017
//	Descrizione:		Comando per il settaggio dello stato del processo
//****************************************************************************************************//

// Header del processo
#include <Process/inp.h>

// Header Qt
#include <QRegularExpression>

using namespace std;
using namespace drex::processes;
using namespace drex::state;

// Impostazione stato del processo
void INProcess::commandSetState(const Message &msg) {
	// Cambio lo stato del processo
	_logger->info("Command: Set State");
	
	// Sintassi del comando
	vector<const char *> syntax;
	syntax.push_back("(?i)^ *(?<newstate>IDLE|RUN) *$");
	
	// Verifica della sintassi
	QList<QRegularExpressionMatch> matches;
	if (!verifyCommandSyntax(msg, syntax, matches)) {
		return;
	}
	
	// Estraggo il nuovo stato
	string newstate = matches[0].captured("newstate").toUpper().toStdString();
	
	// Messaggio di risposta
	Message ans;
	ans += msg[0];
	
	// Cambio lo stato
	if (!setState(newstate)) {
		ans += "ERROR";
		ans += "On change state";
		_command_socket.socket->sendMessage(ans);
		return;
	}
	
	// Invio la risposta
	ans += "OK";
	_command_socket.socket->sendMessage(ans);
}
