//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		09/08/2017
//	Descrizione:		Salvataggio dell'immagine di una camera
//****************************************************************************************************//

// Header del processo
#include <Process/inp.h>

using namespace std;
using namespace drex::processes;

// Salvataggio dell'ultima immagine acquisita della inspection camera specificata
void INProcess::commandSaveImage(const Message &msg) {
	_logger->info("Command: Save Image");
	
	// Sintassi del comando
	vector<const char *> syntax;
	syntax.push_back("(?i)^ *CAMERA *= *(?<camera>.+) *$");
	
	// Verifica della sintassi
	QList<QRegularExpressionMatch> matches;
	if (!verifyCommandSyntax(msg, syntax, matches)) {
		return;
	}
	
	// Estraggo il percorso dove salvare l'immagine
	QString camera = matches[0].captured("camera");
	
	// Salvo l'immagine
	saveImage(camera);
	
	// Invio la risposta
	Message ans;
	ans += msg[0];
	ans += "OK";
	_command_socket.socket->sendMessage(ans);
}
