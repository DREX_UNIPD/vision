//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		15/09/2017
//	Descrizione:		Inizializzazione dello storage
//****************************************************************************************************//

// Header del processo
#include <Process/inp.h>

using namespace std;
using namespace drex::processes;

// Inizializzo lo storage
bool INProcess::initStorage() {
	// Leggo i parametri dello storage
	const YAML::Node &config_storage = _configuration["Storage"];
	
	// Carico le impostazioni di limite massimo dei video
	_max_video_size = config_storage["MaxVideoSize"].as<uint64_t>();
	_max_video_time = config_storage["MaxVideoTime"].as<uint64_t>();
	
	// Carico le cartelle dello storage e le creo se non esistono
	auto create_dir = [this] (QDir &base, string path, string errmsg) {
		QString p = QString::fromStdString(path);
		if (!base.cd(p)) {
			if (!base.mkpath(p)) {
				_console->error(errmsg);
				return false;
			}
			base.cd(p);
		}
		return true;
	};
	
	_storage_ssd_dir = QDir::home();
	_storage_ram_dir = QDir::home();
	
	if (!create_dir(_storage_ssd_dir, config_storage["SSD"].as<string>(), "Failed to create SSD folder")) return false;
	if (!create_dir(_storage_ram_dir, config_storage["RAM"].as<string>(), "Failed to create RAM folder")) return false;
	
	_storage_image_dir = _storage_ssd_dir;
	_storage_video_dir = _storage_ssd_dir;
	_storage_log_dir = _storage_ssd_dir;
	
	if (!create_dir(_storage_image_dir, config_storage["Image"].as<string>(), "Failed to create Image folder")) return false;
	if (!create_dir(_storage_video_dir, config_storage["Video"].as<string>(), "Failed to create Video folder")) return false;
	if (!create_dir(_storage_log_dir, config_storage["Log"].as<string>(), "Failed to create Log folder")) return false;
	
	return true;
}
