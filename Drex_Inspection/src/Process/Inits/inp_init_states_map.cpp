//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		06/08/2017
//	Descrizione:		Inizializzazione degli stati
//****************************************************************************************************//

// Header del processo
#include <Process/inp.h>

using namespace std;
using namespace drex::processes;
using namespace drex::state;

bool INProcess::initStatesMap() {
	_logger->info("Init states map");
	
	// Popolo la mappa stato -> StateObject
	StateObject *so;
	
	// Elimino l'eventuale contenuto della mappa
	_state_objects.clear();
	
	// DEFAULT
	so = new StateObject();
	so->init = [this] () {};
	so->command.insert(INProcess::CMD_GET_STATE, [this] (const Message &msg) { this->commandGetState(msg); });
	so->command.insert(INProcess::CMD_KILL, [this] (const Message &msg) { this->commandKill(msg); });
	so->command.insert(INProcess::CMD_SET_STATE, [this] (const Message &msg) { this->commandSetState(msg); });
	so->command.insert(INProcess::CMD_SAVE_IMAGE, [this] (const Message &msg) { this->commandSaveImage(msg); });
	so->cleanup = [this] () {};
	_state_objects.insert(INProcess::ST_DEFAULT, so);
	
	// IDLE
	so = new StateObject();
	so->init = [this] () { this->stateIdleInit(); };
	so->cleanup = [this] () { this->stateIdleCleanup(); };
	_state_objects.insert(INProcess::ST_IDLE, so);
	
	// RUN
	so = new StateObject();
	so->init = [this] () { this->stateRunInit(); };
	so->cleanup = [this] () { this->stateRunCleanup(); };
	_state_objects.insert(INProcess::ST_RUN, so);
	
	return true;
}



