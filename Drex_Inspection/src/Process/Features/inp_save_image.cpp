//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		06/08/2017
//	Descrizione:		Richiesta di salvataggio di un'immagine
//****************************************************************************************************//

// Header del processo
#include <Process/inp.h>

using namespace std;
using namespace drex::processes;
using namespace drex::state;

bool INProcess::saveImage(QString name) {
	emit acquireNext(name);
}
