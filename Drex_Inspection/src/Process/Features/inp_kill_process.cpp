//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		06/08/2017
//	Descrizione:		Terminazione del processo
//****************************************************************************************************//

// Header del processo
#include <Process/inp.h>

using namespace drex::processes;

// Metodo per la terminazione del processo
void INProcess::killProcess() {
	_logger->info("Kill process");
	
	// Mando il segnale di chiusura del processo
	QTimer::singleShot(0, this, &INProcess::quit);
}
