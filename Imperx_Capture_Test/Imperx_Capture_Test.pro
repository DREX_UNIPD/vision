TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt
CONFIG += debug_and_release
CONFIG(release, debug|release):DEFINES += QT_NO_DEBUG_OUTPUT PV_NO_GEV1X_PIXEL_TYPES PV_NO_DEPRECATED_PIXEL_TYPES _UNIX_ _LINUX_

SOURCES += main.cpp

INCLUDEPATH += /opt/pleora/ebus_sdk/Ubuntu-x86_64/include
INCLUDEPATH += /opt/imperx/bobcat_gev/include
INCLUDEPATH += /opt/imperx/bobcat_gev/lib
INCLUDEPATH += /opt/imperx/bobcat_gev/share/samples/eBUSPlayer
#INCLUDEPATH += /usr/include/qt4
INCLUDEPATH += /usr/local/lib/GenICam_v2_4
INCLUDEPATH += /usr/local/lib/GenICam_v2_4/bin/Linux

DEPENDPATH += /opt/pleora/ebus_sdk/Ubuntu-x86_64/include
DEPENDPATH += /opt/imperx/bobcat_gev/include
DEPENDPATH += /opt/imperx/bobcat_gev/lib
DEPENDPATH += /opt/imperx/bobcat_gev/share/samples/eBUSPlayer
#DEPENDPATH += /usr/include/qt4
DEPENDPATH += /usr/local/lib/GenICam_v2_4
DEPENDPATH += /usr/local/lib/GenICam_v2_4/bin/Linux

LIBS += -L "/opt/imperx/bobcat_gev/lib/" -lPvDevice -lPvBuffer -lPvStream -lEbTransportLayerLib -lEbUtilsLib
LIBS += -L "/opt/imperx/bobcat_gev/lib/" -lPtConvertersLib -lPtUtilsLib -lPvAppUtils -lPvBase -lPvGenICam -lPvGUI
LIBS += -L "/opt/imperx/bobcat_gev/lib/" -lPvPersistence -lPvSerial -lPvTransmitter -lPtUtilsLib -lPvVirtualDevice
LIBS += -L "/opt/imperx/bobcat_gev/lib/" -lSimpleImagingLib

#LIBS += -L "/opt/pleora/ebus_sdk/Ubuntu-x86_64/lib/" -lPvDevice -lPvBuffer -lPvStream -lEbTransportLayerLib -lEbUtilsLib
#LIBS += -L "/opt/pleora/ebus_sdk/Ubuntu-x86_64/lib/" -lPtConvertersLib -lPtUtilsLib -lPvAppUtils -lPvBase -lPvGenICam
#LIBS += -L "/opt/pleora/ebus_sdk/Ubuntu-x86_64/lib/" -lPvGUI -lPvPersistence -lPvSerial -lPvTransmitter -lPtUtilsLib
#LIBS += -L "/opt/pleora/ebus_sdk/Ubuntu-x86_64/lib/" -lPvVirtualDevice -lSimpleImagingLib

QMAKE_LFLAGS += -Wl,-rpath,"'/$$ORIGIN'"

LIBS += -L$$PWD/../../../../../../opt/imperx/bobcat_gev/lib/genicam/bin/Linux64_x64/ -lGenApi_gcc40_v2_4
LIBS += -L$$PWD/../../../../../../opt/imperx/bobcat_gev/lib/genicam/bin/Linux64_x64/ -lGCBase_gcc40_v2_4
LIBS += -L$$PWD/../../../../../../opt/imperx/bobcat_gev/lib/genicam/bin/Linux64_x64/ -lMathParser_gcc40_v2_4
LIBS += -L$$PWD/../../../../../../opt/imperx/bobcat_gev/lib/genicam/bin/Linux64_x64/ -llog4cpp_gcc40_v2_4
LIBS += -L$$PWD/../../../../../../opt/imperx/bobcat_gev/lib/genicam/bin/Linux64_x64/ -lLog_gcc40_v2_4

INCLUDEPATH += $$PWD/../../../../../../opt/imperx/bobcat_gev/lib/genicam/bin/Linux64_x64
DEPENDPATH += $$PWD/../../../../../../opt/imperx/bobcat_gev/lib/genicam/bin/Linux64_x64
