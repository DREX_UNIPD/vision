//#include <errno.h>
//#include <fcntl.h>
//#include <linux/videodev2.h>
//#include <stdint.h>
//#include <stdio.h>
//#include <string.h>
//#include <sys/ioctl.h>
//#include <sys/mman.h>
//#include <unistd.h>
//#include <opencv2/core/core.hpp>
//#include <opencv2/highgui/highgui.hpp>

//const int IMG_WIDTH = 3856;
//const int IMG_HEIGHT = 2764;
//uint8_t *buffer_0;
//uint8_t *buffer_1;

//static int xioctl(int fd, int request, void *arg) {

//  int r;

//  do r = ioctl (fd, request, arg);

//  while (-1 == r && EINTR == errno);
//    return r;
//  }

//int print_caps(int fd) {

//  struct v4l2_capability caps = {};
//  if (-1 == xioctl(fd, VIDIOC_QUERYCAP, &caps)) {
//    perror("Querying Capabilities");
//    return 1;
//  }

//  printf( "Driver Caps:\n"
//          " Driver: \"%s\"\n"
//          " Card: \"%s\"\n"
//          " Bus: \"%s\"\n"
//          " Version: %d.%d\n"
//          " Capabilities: %08x\n",
//          caps.driver,
//          caps.card,
//          caps.bus_info,
//         (caps.version>>16)&&0xff,
//         (caps.version>>24)&&0xff,
//          caps.capabilities);


//  struct v4l2_cropcap cropcap = {0};
//  cropcap.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

//  if (-1 == xioctl (fd, VIDIOC_CROPCAP, &cropcap)){
//    perror("Querying Cropping Capabilities");
//    return 1;
//  }

//  printf( "Camera Cropping:\n"
//          " Bounds: %dx%d+%d+%d\n"
//          " Default: %dx%d+%d+%d\n"
//          " Aspect: %d/%d\n",
//          cropcap.bounds.width, cropcap.bounds.height, cropcap.bounds.left, cropcap.bounds.top,
//          cropcap.defrect.width, cropcap.defrect.height, cropcap.defrect.left, cropcap.defrect.top,
//          cropcap.pixelaspect.numerator, cropcap.pixelaspect.denominator);

//  int support_grbg10 = 0;

//  struct v4l2_fmtdesc fmtdesc = {0};
//  fmtdesc.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
//  char fourcc[5] = {0};
//  char c, e;
//  printf(" FMT : CE Desc\n--------------------\n");

//  while (0 == xioctl(fd, VIDIOC_ENUM_FMT, &fmtdesc)){
//    strncpy(fourcc, (char *)&fmtdesc.pixelformat, 4);

//    if (fmtdesc.pixelformat == V4L2_PIX_FMT_SGRBG10)
//      support_grbg10 = 1;

//    c = fmtdesc.flags & 1? 'C' : ' ';
//    e = fmtdesc.flags & 2? 'E' : ' ';
//    printf(" %s: %c%c %s\n", fourcc, c, e, fmtdesc.description);
//    fmtdesc.index++;

//  }


//  /*if (!support_grbg10){
//    printf("Doesn't support GRBG10.\n");
//    return 1;
//    }*/

//  struct v4l2_format fmt = {0};
//  fmt.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
//  fmt.fmt.pix.width = 3856;
//  fmt.fmt.pix.height = 2764;
//  //fmt.fmt.pix.pixelformat = V4L2_PIX_FMT_BGR24;
//  fmt.fmt.pix.pixelformat = V4L2_PIX_FMT_GREY;
//  //fmt.fmt.pix.pixelformat = V4L2_PIX_FMT_MJPEG;
//  fmt.fmt.pix.field = V4L2_FIELD_NONE;

//  if (-1 == xioctl(fd, VIDIOC_S_FMT, &fmt)) {
//    perror("Setting Pixel Format");
//    return 1;
//  }

//  strncpy(fourcc, (char *)&fmt.fmt.pix.pixelformat, 4);
//  printf( "Selected Camera Mode:\n"
//          " Width: %d\n"
//          " Height: %d\n"
//          " PixFmt: %s\n"
//          " Field: %d\n",
//          fmt.fmt.pix.width,
//          fmt.fmt.pix.height,
//          fourcc,
//          fmt.fmt.pix.field);

//  return 0;
//}

//int init_mmap(int fd_0, int fd_1){

//  struct v4l2_requestbuffers req_0 = {0};
//  req_0.count = 1;
//  req_0.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
//  req_0.memory = V4L2_MEMORY_MMAP;

//  struct v4l2_requestbuffers req_1 = {0};
//  req_1.count = 1;
//  req_1.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
//  req_1.memory = V4L2_MEMORY_MMAP;


//  if (-1 == xioctl(fd_0, VIDIOC_REQBUFS, &req_0) || -1 == xioctl(fd_1, VIDIOC_REQBUFS, &req_1)){
//    perror("Requesting Buffer");
//    return 1;
//  }

//  struct v4l2_buffer buf_0 = {0};
//  buf_0.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
//  buf_0.memory = V4L2_MEMORY_MMAP;
//  buf_0.index = 0;

//  struct v4l2_buffer buf_1 = {0};
//  buf_1.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
//  buf_1.memory = V4L2_MEMORY_MMAP;
//  buf_1.index = 0;

//  if(-1 == xioctl(fd_0, VIDIOC_QUERYBUF, &buf_0) || -1 == xioctl(fd_1, VIDIOC_QUERYBUF, &buf_1)){
//    perror("Querying Buffer");
//    return 1;
//  }

//  buffer_0 = (uint8_t *) mmap (NULL, buf_0.length, PROT_READ | PROT_WRITE, MAP_SHARED, fd_0, buf_0.m.offset);
//  printf("Length: %d\nAddress: %p\n", buf_0.length, buffer_0);
//  printf("Image Length: %d\n", buf_0.bytesused);

//  buffer_1 = (uint8_t *) mmap (NULL, buf_1.length, PROT_READ | PROT_WRITE, MAP_SHARED, fd_1, buf_1.m.offset);
//  printf("Length: %d\nAddress: %p\n", buf_1.length, buffer_1);
//  printf("Image Length: %d\n", buf_1.bytesused);

//  return 0;
//}

//int capture_image(int fd_0, int fd_1, cv::Mat img_0, cv::Mat img_1){

//  struct v4l2_buffer buf_0 = {0};
//  buf_0.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
//  buf_0.memory = V4L2_MEMORY_MMAP;
//  buf_0.index = 0;

//  struct v4l2_buffer buf_1 = {0};
//  buf_1.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
//  buf_1.memory = V4L2_MEMORY_MMAP;
//  buf_1.index = 0;

//  if(-1 == xioctl(fd_0, VIDIOC_QBUF, &buf_0) || -1 == xioctl(fd_1, VIDIOC_QBUF, &buf_1)){
//    perror("Query Buffer");
//    return 1;
//  }

//  if(-1 == xioctl(fd_0, VIDIOC_STREAMON, &buf_0.type) || -1 == xioctl(fd_1, VIDIOC_STREAMON, &buf_1.type)){
//    perror("Start Capture");
//    return 1;
//  }

//  fd_set fds_0;
//  FD_ZERO(&fds_0);
//  FD_SET(fd_0, &fds_0);
//  struct timeval tv_0 = {0};
//  tv_0.tv_sec = 0;
//  int r_0 = select(fd_0+1, &fds_0, NULL, NULL, &tv_0);

//  fd_set fds_1;
//  FD_ZERO(&fds_1);
//  FD_SET(fd_1, &fds_1);
//  struct timeval tv_1 = {0};
//  tv_1.tv_sec = 0;
//  int r_1 = select(fd_1+1, &fds_1, NULL, NULL, &tv_1);

//  if(-1 == r_0 || -1 == r_1){
//    perror("Waiting for Frame");
//    return 1;
//  }

//  if(-1 == xioctl(fd_0, VIDIOC_DQBUF, &buf_0) || -1 == xioctl(fd_1, VIDIOC_DQBUF, &buf_1)){
//    perror("Retrieving Frame");
//    return 1;
//  }
//// printf ("saving image\n");

//  img_0 = cv::Mat(2764, 3856, CV_8U, (void *)buffer_0);
//  img_1 = cv::Mat(2764, 3856, CV_8U, (void *)buffer_1);

//  cv::namedWindow("window 0", CV_WINDOW_NORMAL);
//  cv::imshow("window 0", img_0);
//  cv::namedWindow("window 1", CV_WINDOW_NORMAL);
//  cv::imshow("window 1", img_1);

//  cv::waitKey(0);

////  std::vector<int> compression_params;
////  compression_params.push_back(CV_IMWRITE_JPEG_QUALITY);
////  compression_params.push_back(85);

//  cv::imwrite("image_0.bmp", img_0/*, compression_params*/);
//  cv::imwrite("image_1.bmp", img_1/*, compression_params*/);

//  return 0;

//  }

//int main() {

//  int fd_0;
//  int fd_1;

//  cv::Mat img_0, img_1;

//  fd_0 = open("/dev/video0", O_RDWR);
//  fd_1 = open("/dev/video1", O_RDWR);

//  if (fd_0 == -1 || fd_1 == -1){
//    perror("Opening video device");
//    return 1;
//  }

//  if(print_caps(fd_0) || print_caps(fd_1))
//    return 1;

//  if(init_mmap(fd_0, fd_1))
//    return 1;

////  int i;

//  while(true){

//      if(capture_image(fd_0, fd_1, img_0, img_1))
//        return 1;

//      close(fd_0);
//      close(fd_1);

//  }

//  return 0;
//}

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <linux/ioctl.h>
#include <linux/types.h>
#include <linux/v4l2-common.h>
#include <linux/v4l2-controls.h>
#include <linux/videodev2.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <string.h>
#include <fstream>
#include <string>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

using namespace std;

int main() {

  // 1. Open the device
  int fd_0, fd_1; // A file descriptor to the video device

  fd_0 = open("/dev/video0",O_RDWR);
  fd_1 = open("/dev/video1",O_RDWR);

  cv::Mat img_0, img_1;

  if((fd_0 < 0)||(fd_1 < 0)){
    perror("Failed to open device, OPEN");
    return 1;
  }


  // 2. Ask the device if it can capture frames
  v4l2_capability capability;

  if((ioctl(fd_0, VIDIOC_QUERYCAP, &capability) < 0)||(ioctl(fd_1, VIDIOC_QUERYCAP, &capability) < 0)){
    // something went wrong... exit
    perror("Failed to get device capabilities, VIDIOC_QUERYCAP");
    return 1;
  }


  // 3. Set Image format
  v4l2_format imageFormat0;
  imageFormat0.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  imageFormat0.fmt.pix.width = 3856;
  imageFormat0.fmt.pix.height = 2764;
  imageFormat0.fmt.pix.pixelformat = V4L2_PIX_FMT_MJPEG;
  imageFormat0.fmt.pix.field = V4L2_FIELD_NONE;

  v4l2_format imageFormat1;
  imageFormat1.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  imageFormat1.fmt.pix.width = 3856;
  imageFormat1.fmt.pix.height = 2764;
  imageFormat1.fmt.pix.pixelformat = V4L2_PIX_FMT_MJPEG;
  imageFormat1.fmt.pix.field = V4L2_FIELD_NONE;

  // tell the device you are using this format
  if((ioctl(fd_0, VIDIOC_S_FMT, &imageFormat0) < 0)||(ioctl(fd_1, VIDIOC_S_FMT, &imageFormat1) < 0)){
    perror("Device could not set format, VIDIOC_S_FMT");
    return 1;
  }


  // 4. Request Buffers from the device
  v4l2_requestbuffers requestBuffer0 = {0};
  requestBuffer0.count = 1; // one request buffer
  requestBuffer0.type = V4L2_BUF_TYPE_VIDEO_CAPTURE; // request a buffer wich we an use for capturing frames
  requestBuffer0.memory = V4L2_MEMORY_MMAP;

  v4l2_requestbuffers requestBuffer1 = {0};
  requestBuffer1.count = 1; // one request buffer
  requestBuffer1.type = V4L2_BUF_TYPE_VIDEO_CAPTURE; // request a buffer wich we an use for capturing frames
  requestBuffer1.memory = V4L2_MEMORY_MMAP;

  if((ioctl(fd_0, VIDIOC_REQBUFS, &requestBuffer0) < 0)||(ioctl(fd_1, VIDIOC_REQBUFS, &requestBuffer1) < 0)){
    perror("Could not request buffer from device, VIDIOC_REQBUFS");
    return 1;
  }

  // 5. Quety the buffer to get raw data ie. ask for the you requested buffer
  //    and allocate memory for it
  v4l2_buffer queryBuffer0 = {0};
  queryBuffer0.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  queryBuffer0.memory = V4L2_MEMORY_MMAP;
  queryBuffer0.index = 0;

  v4l2_buffer queryBuffer1 = {0};
  queryBuffer1.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  queryBuffer1.memory = V4L2_MEMORY_MMAP;
  queryBuffer1.index = 0;

  if((ioctl(fd_0, VIDIOC_QUERYBUF, &queryBuffer0) < 0)||(ioctl(fd_1, VIDIOC_QUERYBUF, &queryBuffer1) < 0)){
    perror("Device did not return the buffer information, VIDIOC_QUERYBUF");
    return 1;
  }
  // use a pointer to point to the newly created buffer
  // mmap() will map the memory address of the device to
  // an address in memory
  char* buffer0 = (char*)mmap(NULL, queryBuffer0.length, PROT_READ | PROT_WRITE, MAP_SHARED, fd_0, queryBuffer0.m.offset);
  memset(buffer0, 0, queryBuffer0.length);

  char* buffer1 = (char*)mmap(NULL, queryBuffer1.length, PROT_READ | PROT_WRITE, MAP_SHARED, fd_1, queryBuffer1.m.offset);
  memset(buffer1, 0, queryBuffer1.length);


  // 6. Get a frame
  //    Create a new buffer type so the device knows which buffer we are talking about
  v4l2_buffer bufferinfo0;
  memset(&bufferinfo0, 0, sizeof(bufferinfo0));
  bufferinfo0.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  bufferinfo0.memory = V4L2_MEMORY_MMAP;
  bufferinfo0.index = 0;

  v4l2_buffer bufferinfo1;
  memset(&bufferinfo1, 0, sizeof(bufferinfo1));
  bufferinfo1.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  bufferinfo1.memory = V4L2_MEMORY_MMAP;
  bufferinfo1.index = 0;

  // Activate streaming
  int type0 = bufferinfo0.type;
  int type1 = bufferinfo1.type;
  if((ioctl(fd_0, VIDIOC_STREAMON, &type0) < 0) || (ioctl(fd_1, VIDIOC_STREAMON, &type1) < 0)){
    perror("Could not start streaming, VIDIOC_STREAMON");
    return 1;
  }

  /***************************** Begin looping here *********************/

  while (true){

    // Queue the buffer
    if((ioctl(fd_0, VIDIOC_QBUF, &bufferinfo0) < 0)||(ioctl(fd_1, VIDIOC_QBUF, &bufferinfo1) < 0)){
      perror("Could not queue buffer, VIDIOC_QBUF");
      return 1;
    }

    // Dequeue the buffer
    if((ioctl(fd_0, VIDIOC_DQBUF, &bufferinfo0) < 0)||(ioctl(fd_1, VIDIOC_DQBUF, &bufferinfo1) < 0)){
      perror("Could not dequeue the buffer, VIDIOC_DQBUF");
      return 1;
    }
    // Frames get written after dequeuing the buffer

    cout << "Buffer has: " << (double)bufferinfo0.bytesused / 1024<< " KBytes of data" << endl;
    cout << "Buffer has: " << (double)bufferinfo1.bytesused / 1024<< " KBytes of data" << endl;


    // Write the data out to file
    // ofstream outFile;
    // outFile.open("webcam_output.jpeg", ios::binary| ios::app);

    img_0 = cv::Mat(2764, 3856, CV_8U, (void *)buffer0);
    img_1 = cv::Mat(2764, 3856, CV_8U, (void *)buffer1);

    cv::namedWindow("window 0", CV_WINDOW_NORMAL);
    cv::imshow("window 0", img_0);
    cv::namedWindow("window 1", CV_WINDOW_NORMAL);
    cv::imshow("window 1", img_1);

    cv::waitKey(0);

    cv::imwrite("image_0.bmp", img_0);
    cv::imwrite("image_1.bmp", img_1);

//    int bufPos = 0, outFileMemBlockSize = 0; // the position in the buffer and the amount to copy from
//                                             // the buffer

//    int remainingBufferSize0 = bufferinfo0.bytesused; // the remaining buffer size, is decremented by
//    // memBlockSize amount on each loop so we do not overwrite the buffer
//    int remainingBufferSize1 = bufferinfo1.bytesused; // the remaining buffer size, is decremented by
//    // memBlockSize amount on each loop so we do not overwrite the buffer

//    char* outFileMemBlock = NULL; // a pointer to a new memory block

//    int itr = 0; // counts the number of iterations

    // SECONDO ME QUESTO CICLO NON SERVE

//    while(remainingBufferSize > 0) {

//      bufPos += outFileMemBlockSize; // increment the buffer pointer on each loop
//      // initialise bufPos before outFileMemBlockSize so we can start
//      // at the begining of the buffer

//      outFileMemBlockSize = 1024; // set the output block size to a preferable size. 1024 :)
//      outFileMemBlock = new char[sizeof(char) * outFileMemBlockSize];

//      // copy 1024 bytes of data starting from buffer+bufPos
//      memcpy(outFileMemBlock, buffer0+bufPos, outFileMemBlockSize);
//      outFile.write(outFileMemBlock,outFileMemBlockSize);

//      // calculate the amount of memory left to read
//      // if the memory block size is greater than the remaining
//      // amount of data we have to copy
//      if(outFileMemBlockSize > remainingBufferSize0)
//      outFileMemBlockSize = remainingBufferSize0;

//      // subtract the amount of data we have to copy
//      // from the remaining buffer size
//      remainingBufferSize0 -= outFileMemBlockSize;

//      // display the remaining buffer size
//      cout << itr++ << " Remaining bytes: "<< remainingBufferSize0 << endl;

//    }

  // Close the file
  // outFile.close();


  /******************************** end looping here **********************/

  // end streaming
  if((ioctl(fd_0, VIDIOC_STREAMOFF, &type0) < 0)||(ioctl(fd_1, VIDIOC_STREAMOFF, &type1) < 0)){
    perror("Could not end streaming, VIDIOC_STREAMOFF");
    return 1;
  }

  close(fd_0);
  close(fd_1);

  }
  return 0;

}
