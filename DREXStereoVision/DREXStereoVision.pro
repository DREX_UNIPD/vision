TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    stereomatch.cpp \
    rototranslation.cpp

HEADERS += \
    ../../../QtCreator/Projects/DREXStereoVision/StereoVision.h \
    StereoVision.h

LIBS += -lopencv_core
LIBS += -lopencv_imgcodecs
LIBS += -lopencv_highgui
LIBS += -lopencv_imgproc
LIBS += -lopencv_calib3d
LIBS += /usr/local/lib/libaruco.so
