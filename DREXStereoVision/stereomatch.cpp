#include "StereoVision.h"

using namespace std;
using namespace cv;
using namespace aruco;

Mat resize(const Mat &in,int width){
  if (in.size().width<=width) return in;
  float yf=float(width)/float(in.size().width);
  Mat im2;
  resize(in,im2,Size(width,float(in.size().height)*yf));
  return im2;
}

void stereomatch(Mat Image_1, Mat Image_2, Mat P_matrix_1, Mat P_matrix_2, Mat AR_1, Mat AR_2){

  MarkerDetector MDetector;                    // Definition of Markers detection function
  vector<Marker> TheMarkers_1 ,TheMarkers_2;   // vector including information of Markers:
                                               //   ID, coordinates in pixel of the corners and Tvec/Rvec
  CameraParameters TheCameraParameters_1;
  CameraParameters TheCameraParameters_2;      // input of parameters of cameras

  if (Image_1.empty() || Image_2.empty() )
     cerr << "Could not load image" << endl;


  // Takes intrinsics of cameras
  // Point2d centre_1 = Point2f(320,240);
  TheCameraParameters_1.CameraMatrix = (Mat_<double>(3,3) << 4.7542965295768745e+03, 0., 1.5682253342923068e+03,
                                                             0., 4.7822689461863984e+03, 1.4446192507661333e+03,
                                                             0., 0., 1.);
  // Point2d centre_2 = Point2f(320,240);
  TheCameraParameters_2.CameraMatrix = (Mat_<double>(3,3) << 4.7542965295768745e+03, 0., 1.5682253342923068e+03,
                                                             0., 4.7822689461863984e+03, 1.4446192507661333e+03,
                                                             0., 0., 1.);

  if (TheCameraParameters_1.isValid())
    TheCameraParameters_1.resize(Image_1.size());
  if (TheCameraParameters_2.isValid())
    TheCameraParameters_2.resize(Image_1.size());

  // Markers dictionary
  MDetector.setDictionary("ARUCO_MIP_36h12");

  // Thresholding parameters
  MDetector.setThresholdParams(7, 7);
  MDetector.setThresholdParamRange(2, 0);

  // Lenght of the side of the Markers (detection needs it, but its implications are not used).
  float TheMarkerSize = stof("0.010");

  // Detection of Markers in the images
  TheMarkers_1 = MDetector.detect(Image_1, TheCameraParameters_1, TheMarkerSize);
  TheMarkers_2 = MDetector.detect(Image_2, TheCameraParameters_2, TheMarkerSize);

  // Number of Markers detected in the first image
  cout << "Number of detected AR Markers in the first image = " << TheMarkers_1.size() << endl;
  // Prints infos of the corners of the Markers in the first image
//  for (unsigned int i = 0; i < TheMarkers_1.size(); i++) {
//     cout << "ID =" << TheMarkers_1[i].id << endl << " N =" << TheMarkers_1[i][0] << endl
//          << " E =" << TheMarkers_1[i][1] << endl << " S =" << TheMarkers_1[i][2] << endl
//          << " W =" << TheMarkers_1[i][3] << endl;
//  }

  // Number of Markers detected in the second image
  cout << "Number of detected AR Markers in the second image = " << TheMarkers_2.size() << endl;
  // Prints infos of the corners of the Markers in the second image
//  for (unsigned int i = 0; i < TheMarkers_2.size(); i++) {
//     cout << "ID =" << TheMarkers_2[i].id << endl << " N =" << TheMarkers_2[i][0] << endl
//          << " E =" << TheMarkers_2[i][1] << endl << " S =" << TheMarkers_2[i][2] << endl
//          << " W =" << TheMarkers_2[i][3] << endl;
//  }

  waitKey(0);

  // Definition of the vectors including the centres of the Markers: they must have at the same index the Marker
  // with the same ID.
  // Goes along the first vector and for each Marker look for the one with the same ID in the second one,
  // writes in the i-th position of both vector coordinates of the centre of the Marker.
  vector <Point2f> ARcentres_1, ARcentres_2;
  for (unsigned int i = 0; i < TheMarkers_1.size(); i++){
      for (unsigned int j = 0; j < TheMarkers_2.size(); j++){
          if (TheMarkers_1[i].id == TheMarkers_2[j].id)
            ARcentres_1.push_back(Point2f(((TheMarkers_1[j][0].x + TheMarkers_1[j][2].x)/2),
                                         (((TheMarkers_1[j][0].y + TheMarkers_1[j][2].y)/2))));
      }
   }
  for (unsigned int i = 0; i < TheMarkers_2.size(); i++){
      for (unsigned int j = 0; j < TheMarkers_1.size(); j++){
          if (TheMarkers_2[i].id == TheMarkers_1[j].id)
            ARcentres_2.push_back(Point2f(((TheMarkers_2[j][0].x + TheMarkers_2[j][2].x)/2),
                                         (((TheMarkers_2[j][0].y + TheMarkers_2[j][2].y)/2))));
      }
   }
//  for (unsigned int i = 0; i < TheMarkers_2.size(); i++){
//      ARcentres_2.push_back(Point2f(((TheMarkers_2[i][0].x + TheMarkers_2[i][2].x)/2),
//                                   (((TheMarkers_2[i][0].y + TheMarkers_2[i][2].y)/2))));
//    }

  AR_1 = Mat(ARcentres_1, CV_64FC2);
  AR_2 = Mat(ARcentres_2, CV_64FC2);
  cout << "AR Markers in the first image =" << endl << AR_1 << endl
       << "AR Markers in the second image =" << endl << AR_2 << endl;

  waitKey(0);

  // 3D points vector
  Mat Points3D = Mat(4, TheMarkers_1.size(), CV_64FC4);

  triangulatePoints(P_matrix_1, P_matrix_2, AR_1, AR_2, Points3D);
  
  vector<Point3d> points;
  for (int i=0; i < Points3D.cols; i++){
	  double x,y,z,w;
	  Mat col = Points3D.col(i);
	  w = col.at<double>(3,0);
	  
	  x = col.at<double>(0, 0) / w;
	  y = col.at<double>(1, 0) / w;
	  z = col.at<double>(2, 0) / w;
	  
	  points.push_back(Point3d(x, y, z));
	  
	  cout << "x = " << x << ", y = " << y << ", z = " << z << endl;
	  	  
  }
  
    
  cout << "3D points = " << endl << Points3D << endl;

}
