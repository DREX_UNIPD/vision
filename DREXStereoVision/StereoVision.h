// intestazione per tutte le funzioni della stereo visione

#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <aruco/aruco.h>
#include <aruco/cvdrawingutils.h>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/calib3d/calib3d.hpp>

void rototranslation(const cv::Mat &, const cv::Mat &, cv::Mat &, cv::Mat &);
void stereomatch(cv::Mat, cv::Mat, cv::Mat, cv::Mat, cv::Mat, cv::Mat);


