// Main

#include "StereoVision.h"

using namespace std;
using namespace cv;
using namespace aruco;


/************************************
 *
 *  Beginning of main function
 *
 ************************************/

int main() {

  Mat TheInputImage_1;    // input of images
  Mat TheInputImage_2;    //
  Mat P_matrix_1, P_matrix_2;
  Mat AR_1, AR_2;

  // Loads images
  TheInputImage_1 = imread("1_left.jpg");
  TheInputImage_2 = imread("1_right.jpg");

  // Calculates relative rototranslation between cameras
  rototranslation(TheInputImage_1, TheInputImage_2, P_matrix_1, P_matrix_2);

  // Traingulation of AR Markers
  stereomatch(TheInputImage_1, TheInputImage_2, P_matrix_1, P_matrix_2, AR_1, AR_2);

  return 0;
}
