// Takes at the input two images for the detection of the scalebar
// The output is the Fundamental Matrices of the cameras.

#include "StereoVision.h"

using namespace std;
using namespace cv;
using namespace aruco;

void rototranslation(const Mat &Image_1, const Mat &Image_2, Mat &P_matrix_1, Mat &P_matrix_2){

  // Check if the images are loaded
  if (Image_1.empty() || Image_2.empty())
    cerr << "Could not load image" << endl;

  namedWindow("Left image", CV_WINDOW_NORMAL);
  imshow("Left image", Image_1);
  namedWindow("Right image", CV_WINDOW_NORMAL);
  imshow("Right image", Image_2);
  waitKey(0);

  // Threshold
  Mat ThreshImage_1, ThreshImage_2;

  // Definitions of contours of the scalebar
  vector<vector<Point>> contours_1, contours_2;

  // Convertion in grayscale
  Mat gray_1, gray_2;
  cvtColor(Image_1, gray_1, CV_BGR2GRAY, 0);
  cvtColor(Image_2, gray_2, CV_BGR2GRAY, 0);

  // Compute mask (you could use a simple threshold if the image is always as good as the one you provided)
  Mat mask_1, mask_2;
  adaptiveThreshold(gray_1, ThreshImage_1, 255, ADAPTIVE_THRESH_MEAN_C, THRESH_BINARY, 105, 1);
  erode(ThreshImage_1, mask_1, Mat(), Point(-1,-1), 3, BORDER_CONSTANT, 1);
  imwrite("Thresh_1.bmp", ThreshImage_1);
  imshow("Left image", ThreshImage_1);
  adaptiveThreshold(gray_2, ThreshImage_2, 255, ADAPTIVE_THRESH_MEAN_C, THRESH_BINARY, 105, 1);
  erode(ThreshImage_2, mask_2, Mat(), Point(-1,-1), 3, BORDER_CONSTANT, 1);
  imwrite("Thresh_2.bmp", ThreshImage_2);
  imshow("Right image", ThreshImage_2);

  waitKey(0);

  // Regions of Interest containing the scalebars
  Mat roi_1 = mask_1(Rect(600, 1700, 1800, 700));
  imwrite("roi_1.bmp", roi_1);
  imshow("Left image", roi_1);

  Mat roi_2 = mask_2(Rect(1300, 1800, 1800, 700));
  imwrite("roi_2.bmp", roi_2);
  imshow("Right image", roi_2);

  // Find contours (if always so easy to segment as your image,
  //                you could just add the black/rect pixels to a vector)
  vector<Vec4i> hierarchy;
  Point Offset_1 = Point(600, 1700);
  findContours(roi_1, contours_1, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE, Offset_1);
  Point Offset_2 = Point(1300, 1800);
  findContours(roi_2, contours_2, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE, Offset_2);

  // Draws the contours and detects the largest one (if there are multiple contours in the image,
  // the largest one is taken as the wanted one). Drawing only for demonstration.
  Mat drawing_1 = Mat::zeros(roi_1.size(), CV_8UC3);
  Mat drawing_2 = Mat::zeros(roi_2.size(), CV_8UC3);
  int biggestContourIdx_1 = -1, biggestContourIdx_2 = -1;
  float biggestContourArea_1 = 0, biggestContourArea_2 = 0;
  //vector<vector<Point> > contours_poly(contours.size());

  // Draws contours of the scalebars.
  for(unsigned int i = 0; i < contours_1.size(); i++ ){
      Scalar color = Scalar(255, 255, 255);
      drawContours(drawing_1, contours_1, i, color, 1, 8, hierarchy, 0, Point());
      double ctArea_1 = contourArea(contours_1[i]);
      if(ctArea_1 > biggestContourArea_1){
          biggestContourArea_1 = ctArea_1;
          biggestContourIdx_1 = i;
      }
  }

  for(unsigned int i = 0; i < contours_2.size(); i++ ){
      Scalar color = Scalar(255, 255, 255);
      drawContours(drawing_2, contours_2, i, color, 1, 8, hierarchy, 0, Point());
      double ctArea_2 = contourArea(contours_2[i]);
      if(ctArea_2 > biggestContourArea_2){
          biggestContourArea_2 = ctArea_2;
          biggestContourIdx_2 = i;
      }
  }

  // Check if no contour is detected.
  if(biggestContourIdx_1 < 0 || biggestContourIdx_2 < 0)
      cout << "No contour found" << endl;

  // Calculate the rotated bounding rectangle of the biggest contours.
  RotatedRect boundingBox_1 = minAreaRect(contours_1[biggestContourIdx_1]);
  RotatedRect boundingBox_2 = minAreaRect(contours_2[biggestContourIdx_2]);

  // NB: Calculate the EXTERNAL boundary box, in case erode/dilate can be used to define better images

  // Draws the rotated rectangle.
  Point2f corners_1[4];
  boundingBox_1.points(corners_1);
  line(drawing_1, corners_1[0], corners_1[1], Scalar(255,255,255));
  line(drawing_1, corners_1[1], corners_1[2], Scalar(255,255,255));
  line(drawing_1, corners_1[2], corners_1[3], Scalar(255,255,255));
  line(drawing_1, corners_1[3], corners_1[0], Scalar(255,255,255));
  for(unsigned int i = 0; i <= 3; i++)
  cout << "corners 1 =" << corners_1[i] << endl;
  imwrite("drawing_1.bmp", drawing_1);

  Point2f corners_2[4];
  boundingBox_2.points(corners_2);
  line(drawing_2, corners_2[0], corners_2[1], Scalar(255,255,255));
  line(drawing_2, corners_2[1], corners_2[2], Scalar(255,255,255));
  line(drawing_2, corners_2[2], corners_2[3], Scalar(255,255,255));
  line(drawing_2, corners_2[3], corners_2[0], Scalar(255,255,255));
  for(unsigned int i = 0; i <= 3; i++)
  cout << "corners 2 =" << corners_2[i] << endl;
  imwrite("drawing_2.bmp", roi_2);

  waitKey(0);

  // display
//  imshow("input", roi_1);
//  imshow("drawing", drawing_1);

//  imshow("input", roi_2);
//  imshow("drawing", drawing_2);

  // Calculates the rototranslation matrices of cameras.
//  Mat rvec_1 = Mat::zeros(3, 1, CV_64FC1), rvec_2 = Mat::zeros(3, 1, CV_64FC1);
//  Mat tvec_1 = Mat::zeros(3, 1, CV_64FC1), tvec_2 = Mat::zeros(3, 1, CV_64FC1);
//  Mat R_matrix_1 = Mat::zeros(3, 3, CV_64FC1), R_matrix_2 = Mat::zeros(3, 3, CV_64FC1);
//  bool useExtrinsicGuess = false;   // se true, solvePnP itera prendendo rvec e tvec come parametri iniziali
//  Mat P_matrix_1 = Mat::zeros(3, 4, CV_64FC1), P_matrix_2 = Mat::zeros(3, 4, CV_64FC1);

  // Coordinates in pixel of the corners of the scalebars, the origin is on the bottom left corner.
  int nimages = 2;

  vector<vector<Point3f>> vector_points3d;
  vector_points3d.resize(nimages);
  for(int i = 0; i < nimages; i++){
      vector_points3d[i].push_back(Point3f(0.0f, 14.0f, 0));
      vector_points3d[i].push_back(Point3f(0.0f, 0.0f, 0));
      vector_points3d[i].push_back(Point3f(145.0f, 0.0f, 0));
      vector_points3d[i].push_back(Point3f(145.0f, 14.0f, 0));
  }
//  vector<Point3f> list_points3d;
//  list_points3d.push_back(Point3f(0.0f, 15.1f, 0));
//  list_points3d.push_back(Point3f(0.0f, 0.0f, 0));
//  list_points3d.push_back(Point3f(148.8f, 0.0f, 0));
//  list_points3d.push_back(Point3f(148.8f, 15.1f, 0));


  // Coordinates in pixel of the detected 2D points of the scalebars.
  vector<vector<Point2f>> vector_points2d_1;
  vector_points2d_1.resize(nimages);
  for(int i = 0; i < nimages; i++){
      vector_points2d_1[i].push_back(corners_1[0]);
      vector_points2d_1[i].push_back(corners_1[1]);
      vector_points2d_1[i].push_back(corners_1[2]);
      vector_points2d_1[i].push_back(corners_1[3]);
  }
//  vector<Point2f> list_points2d_1;
//  list_points2d_1.push_back(corners_1[0]);
//  list_points2d_1.push_back(corners_1[1]);
//  list_points2d_1.push_back(corners_1[2]);
//  list_points2d_1.push_back(corners_1[3]);

  vector<vector<Point2f>> vector_points2d_2;
  vector_points2d_2.resize(nimages);
  for(int i = 0; i < nimages; i++){
      vector_points2d_2[i].push_back(corners_2[0]);
      vector_points2d_2[i].push_back(corners_2[1]);
      vector_points2d_2[i].push_back(corners_2[2]);
      vector_points2d_2[i].push_back(corners_2[3]);
  }
//  vector<Point2f> list_points2d_2;
//  list_points2d_2.push_back(corners_2[0]);
//  list_points2d_2.push_back(corners_2[1]);
//  list_points2d_2.push_back(corners_2[2]);
//  list_points2d_2.push_back(corners_2[3]);


  // Camera internals
  // double focal_length = 8;                    // Approximate focal length.
  // Point2d centre_1 = Point2f(1928, 1382);

  Mat K_matrix_1 = (Mat_<double>(3,3) << 4.7542965295768745e+03, 0., 1.5682253342923068e+03,
                                         0., 4.7822689461863984e+03, 1.4446192507661333e+03,
                                         0., 0., 1.);
  // Point2d centre_2 = Point2f(320,240);
  Mat K_matrix_2 = (Mat_<double>(3,3) << 4.7542965295768745e+03, 0., 1.5682253342923068e+03,
                                         0., 4.7822689461863984e+03, 1.4446192507661333e+03,
                                         0., 0., 1.);

  // coefficienti di distorsione della lente
  Mat distCoeffs_1 = (Mat_<double>(5,1) << -1.1072164198330427e-01, -1.9169101637647601e-01,
                                            5.3038270098660096e-03, -5.0415850443485963e-03,
                                            2.2524458690489454e-01 );


  Mat distCoeffs_2 = (Mat_<double>(5,1) << -1.1072164198330427e-01, -1.9169101637647601e-01,
                                            5.3038270098660096e-03, -5.0415850443485963e-03,
                                            2.2524458690489454e-01 );

//  // trasforma il vettore rotazione in matrice rotazione
//  Rodrigues(rvec_1, R_matrix_1);
//  _t_matrix = tvec;
//  Rodrigues(rvec_2, R_matrix_2);

  Mat R, T, E, F;
  Mat R1, R2, Q;
//  Mat cameraMatrix[2];
//      cameraMatrix[0] = initCameraMatrix2D(vector_points3d, vector_points2d_1, Size(3856, 2764), 0);
//      cameraMatrix[1] = initCameraMatrix2D(vector_points3d, vector_points2d_2, Size(3856, 2764), 0);

//  cout << "Camera 1 = " << cameraMatrix[0] << endl;
//  cout << "Camera 2 = " << cameraMatrix[1] << endl;

  double rms = stereoCalibrate(vector_points3d, vector_points2d_1, vector_points2d_2, K_matrix_1, distCoeffs_1,
                  K_matrix_2, distCoeffs_2, Size(3856, 2764), R, T, E, F, CALIB_FIX_ASPECT_RATIO
                  + CALIB_ZERO_TANGENT_DIST + CALIB_USE_INTRINSIC_GUESS + CALIB_FIX_INTRINSIC,
                  TermCriteria(TermCriteria::COUNT+TermCriteria::EPS, 30, 1e-6));
  cout << "RMS error = " << rms << endl;

  stereoRectify(K_matrix_1, distCoeffs_1, K_matrix_2, distCoeffs_2 , Size(3856, 2764),
                R, T, R1, R2, P_matrix_1, P_matrix_2, Q, /*CALIB_ZERO_DISPARITY,*/ 1);

  cout << "P1 = " << P_matrix_1 << endl
       << "P2 = " << P_matrix_2 << endl
       << "Q = " << Q << endl;

  waitKey(0);

// F = findFundamentalMat(list_points2d_1, list_points2d_2, FM_RANSAC, 3, 0.99);
// cout << "F = " << F << endl;


  // prepara le matrici estrinseche delle due camere
//  for (double i = 0; i < 3; i++){
//    for (double j = 0; j < 4; j++){
//        if (j < 3)
//          P_matrix_1.at<double>(i, j) = R_matrix_1.at<double>(i, j);
//        else
//          P_matrix_1.at<double>(i, j) = tvec_1.at<double>(i);
//      }
//  }
//  for (double i = 0; i < 3; i++){
//    for (double j = 0; j < 4; j++){
//        if (j < 3)
//          P_matrix_2.at<double>(i, j) = R_matrix_2.at<double>(i, j);
//        else
//          P_matrix_2.at<double>(i, j) = tvec_2.at<double>(i);
//      }
//  }

  // calcola le matrici fondamentali delle camere come prodotto tra la matrice intrinseca e quella estrinseca
//  F_matrix_1 = K_matrix_1 * P_matrix_1;
//  F_matrix_2 = K_matrix_2 * P_matrix_2;
}
