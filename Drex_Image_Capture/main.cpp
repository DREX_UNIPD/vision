#include <errno.h>
#include <fcntl.h>
#include <linux/videodev2.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <unistd.h>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/videoio/videoio.hpp>

const int IMG_WIDTH = 3856;
const int IMG_HEIGHT = 2764;

uint8_t *buffer_0;
uint8_t *buffer_1;

static int xioctl(int fd, int request, void *arg) {
	int r;

	do r = ioctl (fd, request, arg);
	while (-1 == r && EINTR == errno);

	return r;
}

int print_caps(int fd) {
	struct v4l2_capability caps = {};
	if (-1 == xioctl(fd, VIDIOC_QUERYCAP, &caps)) {
			perror("Querying Capabilities");
			return 1;
	}

	printf( "Driver Caps:\n"
			"  Driver: \"%s\"\n"
			"  Card: \"%s\"\n"
			"  Bus: \"%s\"\n"
			"  Version: %d.%d\n"
			"  Capabilities: %08x\n",
			caps.driver,
			caps.card,
			caps.bus_info,
			(caps.version>>16)&&0xff,
			(caps.version>>24)&&0xff,
			caps.capabilities);


	struct v4l2_cropcap cropcap = {0};
	cropcap.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	if (-1 == xioctl (fd, VIDIOC_CROPCAP, &cropcap))
	{
			perror("Querying Cropping Capabilities");
			return 1;
	}

	printf( "Camera Cropping:\n"
			"  Bounds: %dx%d+%d+%d\n"
			"  Default: %dx%d+%d+%d\n"
			"  Aspect: %d/%d\n",
			cropcap.bounds.width, cropcap.bounds.height, cropcap.bounds.left, cropcap.bounds.top,
			cropcap.defrect.width, cropcap.defrect.height, cropcap.defrect.left, cropcap.defrect.top,
			cropcap.pixelaspect.numerator, cropcap.pixelaspect.denominator);

	int support_grbg10 = 0;

	struct v4l2_fmtdesc fmtdesc = {0};
	fmtdesc.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	char fourcc[5] = {0};
	char c, e;
	printf("  FMT : CE Desc\n--------------------\n");
	while (0 == xioctl(fd, VIDIOC_ENUM_FMT, &fmtdesc))
	{
			strncpy(fourcc, (char *)&fmtdesc.pixelformat, 4);
			if (fmtdesc.pixelformat == V4L2_PIX_FMT_SGRBG10)
				support_grbg10 = 1;
			c = fmtdesc.flags & 1? 'C' : ' ';
			e = fmtdesc.flags & 2? 'E' : ' ';
			printf("  %s: %c%c %s\n", fourcc, c, e, fmtdesc.description);
			fmtdesc.index++;
	}
	/*
	if (!support_grbg10)
	{
		printf("Doesn't support GRBG10.\n");
		return 1;
	}*/

	struct v4l2_format fmt = {0};
	fmt.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	fmt.fmt.pix.width = 3856;
	fmt.fmt.pix.height = 2764;
	//fmt.fmt.pix.pixelformat = V4L2_PIX_FMT_BGR24;
	fmt.fmt.pix.pixelformat = V4L2_PIX_FMT_GREY;
	//fmt.fmt.pix.pixelformat = V4L2_PIX_FMT_MJPEG;
	fmt.fmt.pix.field = V4L2_FIELD_NONE;

	if (-1 == xioctl(fd, VIDIOC_S_FMT, &fmt)) {
		perror("Setting Pixel Format");
		return 1;
	}

	strncpy(fourcc, (char *)&fmt.fmt.pix.pixelformat, 4);
	printf( "Selected Camera Mode:\n"
		"  Width: %d\n"
		"  Height: %d\n"
		"  PixFmt: %s\n"
		"  Field: %d\n",
		fmt.fmt.pix.width,
		fmt.fmt.pix.height,
		fourcc,
		fmt.fmt.pix.field
	);
	return 0;
}

int init_mmap(int fd_0, int fd_1/*, uint8_t * buffer_0, uint8_t * buffer_1*/)
{
	struct v4l2_requestbuffers req_0 = {0};
	req_0.count = 1;
	req_0.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	req_0.memory = V4L2_MEMORY_MMAP;

	struct v4l2_requestbuffers req_1 = {0};
	req_1.count = 1;
	req_1.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	req_1.memory = V4L2_MEMORY_MMAP;

	if (-1 == xioctl(fd_0, VIDIOC_REQBUFS, &req_0) || -1 == xioctl(fd_1, VIDIOC_REQBUFS, &req_1))
	{
		perror("Requesting Buffer");
		return 1;
	}

	struct v4l2_buffer buf_0 = {0};
	buf_0.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	buf_0.memory = V4L2_MEMORY_MMAP;
	buf_0.index = 0;

	struct v4l2_buffer buf_1 = {0};
	buf_1.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	buf_1.memory = V4L2_MEMORY_MMAP;
	buf_1.index = 0;

	if(-1 == xioctl(fd_0, VIDIOC_QUERYBUF, &buf_0) || -1 == xioctl(fd_1, VIDIOC_QUERYBUF, &buf_1))
	{
		perror("Querying Buffer");
		return 1;
	}

	buffer_0 = (uint8_t *) mmap (NULL, buf_0.length, PROT_READ | PROT_WRITE, MAP_SHARED, fd_0, buf_0.m.offset);
	buffer_1 = (uint8_t *) mmap (NULL, buf_1.length, PROT_READ | PROT_WRITE, MAP_SHARED, fd_1, buf_1.m.offset);
	printf("Length: %d\nAddress: %p\n", buf_0.length, buffer_0);
	printf("Length: %d\nAddress: %p\n", buf_1.length, buffer_1);
	printf("Image Length: %d\n", buf_0.bytesused);
	printf("Image Length: %d\n", buf_1.bytesused);

	return 0;
}

int capture_image(int fd_0, int fd_1/*, cv::Mat img_0, cv::Mat img_1, uint8_t *buffer_0, uint8_t * buffer_1*/)
{
	struct v4l2_buffer buf_0 = {0};
	buf_0.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	buf_0.memory = V4L2_MEMORY_MMAP;
	buf_0.index = 0;
	struct v4l2_buffer buf_1 = {0};
	buf_1.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	buf_1.memory = V4L2_MEMORY_MMAP;
	buf_1.index = 0;
	if(-1 == xioctl(fd_0, VIDIOC_QBUF, &buf_0) || -1 == xioctl(fd_1, VIDIOC_QBUF, &buf_1))
	{
		perror("Query Buffer");
		return 1;
	}

	if(-1 == xioctl(fd_0, VIDIOC_STREAMON, &buf_0.type) || -1 == xioctl(fd_1, VIDIOC_STREAMON, &buf_1.type))
	{
		perror("Start Capture");
		return 1;
	}

	fd_set fds_0;
	fd_set fds_1;
	FD_ZERO(&fds_0);
	FD_ZERO(&fds_1);
	FD_SET(fd_0, &fds_0);
	FD_SET(fd_1, &fds_1);
	struct timeval tv_0 = {0};
	tv_0.tv_sec = 2;
	struct timeval tv_1 = {0};
	tv_1.tv_sec = 2;
	int r_0 = select(fd_0+1, &fds_0, NULL, NULL, &tv_0);
	int r_1 = select(fd_1+1, &fds_1, NULL, NULL, &tv_1);

	if(-1 == r_0 || -1 == r_1)
	{
		perror("Waiting for Frame");
		return 1;
	}

	if(-1 == xioctl(fd_0, VIDIOC_DQBUF, &buf_0) || -1 == xioctl(fd_1, VIDIOC_DQBUF, &buf_1))
	{
		perror("Retrieving Frame");
		return 1;
	}
//	printf ("saving image\n");

	cv::Mat img_0(2764, 3856, CV_8U, (void *)buffer_0);
	cv::Mat img_1(2764, 3856, CV_8U, (void *)buffer_1);

//	std::vector<int> compression_params;
//	compression_params.push_back(CV_IMWRITE_JPEG_QUALITY);
//	compression_params.push_back(85);

//	for (unsigned int i = 0; i < 2; i++)
//	  cv::imwrite("image_" + std::to_string(i) + ".png", img_0/*, compression_params*/);
	cv::imwrite("image_0.png", img_0);
	cv::imwrite("image_1.png", img_1);

	return 0;
}

int main() {
		int fd_0, fd_1;
//		cv::Mat img_0, img_1;
//		uint8_t *buffer_0;
//		uint8_t *buffer_1;

//		cv::Mat frame_0(2764, 3856, CV_8U);
//		cv::VideoCapture capture_0(0);
//		capture_0.set(3, 3856);
//		capture_0.set(4, 2764);

//		cv::Mat frame_1(2764, 3856, CV_8U);
//		cv::VideoCapture capture_1(1);
//		capture_1.set(3, 3856);
//		capture_1.set(4, 2764);
//		int i = 1;

//		while(i == 1){

//		  capture_0 >> frame_0;
//		  capture_1 >> frame_1;

//		  if(frame_0.empty() && frame_1.empty()){
//		    i = 1;
//		    }

//		    else{
//		    cv::imshow("Camera 0", frame_0);
//		    cv::imwrite("image_0.png", frame_0);
//		    cv::imshow("Camera 1", frame_1);
//		    cv::imwrite("image_1.png", frame_1);
//		    cv::waitKey();
//		    i = 0;
//		    }
//		  if(frame_1.empty())
//		    cv::waitKey();


//		  }


			fd_0 = open("/dev/video0", O_RDWR);
			fd_1 = open("/dev/video1", O_RDWR);

			if (fd_0 == -1 || fd_1 == -1) {
					perror("Opening video devices");
					return 1;
			}

			if(print_caps(fd_0) || print_caps(fd_1))
				return 1;


			if(init_mmap(fd_0, fd_1/*, buffer_0, buffer_1*/))
				return 1;

			while(true){

//			    int i = cv::waitKey(0);
	//		    cv::imshow("Camera 0", frame_0);
	//		    cv::VideoCapture::release();

//			    if(i != -1){

			  if(capture_image(fd_0, fd_1/*, img_0, img_1, buffer_0, buffer_1*/)){

//			    cv::imshow("image_0", img_0);
//			    cv::imshow("image_1", img_1);

			      cv::waitKey(30);
			      return 1;
		      }
//		    }
		  }

		close(fd_0);
		close(fd_1);

		return 0;
}
