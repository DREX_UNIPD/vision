#include <stdio.h>
#include <gst/gst.h>

// Simple initialization
int test0(int argc, char *argv[]) {
	const gchar *nano_str;
	guint major, minor, micro, nano;
	
	gst_init (&argc, &argv);
	
	gst_version (&major, &minor, &micro, &nano);
	
	if (nano == 1) nano_str = "(CVS)";
	else if (nano == 2) nano_str = "(Prerelease)";
	else nano_str = "";
	
	printf ("This program is linked against GStreamer %d.%d.%d %s\n", major, minor, micro, nano_str);
	
	return 0;
}

// Creating a GstElement
int test1(int argc, char *argv[]) {
	GstElement *element;
	
	/* init GStreamer */
	gst_init (&argc, &argv);
	
	/* create element */
	element = gst_element_factory_make("fakesrc", "source");
	if (!element) {
		g_print("Failed to create element of type 'fakesrc'\n");
		return -1;
	}
	else g_print("Successfully created element of type 'fakesrc'\n");
	
	gst_object_unref(GST_OBJECT(element));
	
	return 0;
}

// Creating a GstElement (GstElementFactory)
int test2(int argc, char *argv[]) {
	GstElementFactory *factory;
	GstElement * element;
	
	/* init GStreamer */
	gst_init (&argc, &argv);
	
	/* create element, method #2 */
	factory = gst_element_factory_find("fakesrc");
	if (!factory) {
		g_print("Failed to find factory of type 'fakesrc'\n");
		return -1;
	}
	else g_print("Successfully found factory of type 'fakesrc'\n");
	element = gst_element_factory_create(factory, "source");
	if (!element) {
		g_print("Failed to create element, even though its factory exists!\n");
		return -1;
	}
	else g_print("Successfully created element of type 'fakesrc'\n");
	
	gst_object_unref(GST_OBJECT (element));
	gst_object_unref(GST_OBJECT (factory));
	
	return 0;
}

// Using an element as a GObject
int test3(int argc, char *argv[]) {
	GstElement *element;
	gchar *name;
	
	/* init GStreamer */
	gst_init (&argc, &argv);
	
	/* create element */
	element = gst_element_factory_make ("fakesrc", "source");
	
	/* get name */
	g_object_get (G_OBJECT (element), "name", &name, NULL);
	g_print ("The name of the element is '%s'.\n", name);
	g_free (name);
	
	gst_object_unref (GST_OBJECT (element));
	
	return 0;
}

// Getting information about an element using a factory
int test4(int argc, char *argv[]) {
	GstElementFactory *factory;
	
	/* init GStreamer */
	gst_init (&argc, &argv);
	
	/* get factory */
	factory = gst_element_factory_find ("fakesrc");
	if (!factory) {
		g_print ("You don't have the 'fakesrc' element installed!\n");
		return -1;
	}
	
	/* display information */
	g_print ("The '%s' element is a member of the category %s.\nDescription: %s\n",
		gst_plugin_feature_get_name (GST_PLUGIN_FEATURE (factory)),
		gst_element_factory_get_metadata (factory, GST_ELEMENT_METADATA_KLASS),
		gst_element_factory_get_metadata (factory, GST_ELEMENT_METADATA_DESCRIPTION)
	);
	
	gst_object_unref (GST_OBJECT (factory));
	
	return 0;
}

int main(int argc, char *argv[]) {
	return test4(argc, argv);
}

